import { createApp } from 'vue'
import App from './App.vue'

import './assets/main.css'
import './assets/font/Web - Morganite Bold/stylesheet.css'
createApp(App).mount('#app')
